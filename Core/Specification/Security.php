<?php
namespace Ucy\Iss\Component\Ws\Core\Specification;


abstract class Security
{
    const WSSE = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
    const WSU = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';
    const WSUTP = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0';
    const WSMS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0';

    const PASSWORD_DIGEST = 'PasswordDigest';
    const PASSWORD_TEXT = 'PasswordText';

    const BASE64_BINARY = 'Base64Binary';
    const HEX_BINARY = 'HexBinary';

    const WS_SECURITY = 'WS-Security';
}