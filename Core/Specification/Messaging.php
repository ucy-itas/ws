<?php
namespace Ucy\Iss\Component\Ws\Core\Specification;


abstract class Messaging
{
    const WSA = 'http://www.w3.org/2005/08/addressing';

    const WS_ADDRESSING = 'WS-Addressing';
}