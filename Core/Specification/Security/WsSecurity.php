<?php
namespace Ucy\Iss\Component\Ws\Core\Specification\Security;

use Ucy\Iss\Component\Ws\Core\SpecificationInterface;
use Ucy\Iss\Component\Ws\Core\Specification;
use Ucy\Iss\Component\Ws\Core\Specification\Security;

class WsSecurity extends Specification
{
    public function __construct($options)
    {
        if(!isset($options['password_type'])) {
            $options['password_type'] = Security::PASSWORD_TEXT;
        }

        if(!isset($options['ttl'])) {
            $options['ttl'] = 600;
        }

        $this->setName(Security::WS_SECURITY);

        parent::__construct($options);
    }

    /**
     * @return array|\SoapHeader
     */
    public function generateHeaders(array $runtime_options = array())
    {
        $timestamp = gmdate('Y-m-d\TH:i:s\Z');
        $nonce = mt_rand();

        if ($this->options['password_type'] === Security::PASSWORD_DIGEST) {
            $packedNonce = pack('H*', $nonce);
            $packedTimestamp = pack('a*', $timestamp);
            $packedPassword = pack('a*', $this->options['password']);

            $hash = sha1($packedNonce . $packedTimestamp . $packedPassword);
            $packedHash = pack('H*', $hash);

            $password = base64_encode($packedHash);
            $nonce = sha1($nonce);
        } elseif ($this->options['password_type'] === Security::PASSWORD_TEXT) {
            $password = $this->options['password'];
            $nonce = sha1($nonce);
        } else {
            return array();
        }

        $expiration = gmdate('Y-m-d\TH:i:s\Z', (time() + $this->options['ttl']));
        $xml = '<wsse:Security xmlns:wsse="' . Security::WSSE . '" xmlns:wsu="'.Security::WSU . '">' .
            '<wsu:Timestamp wsu:Id="TS-2">' .
            '<wsu:Created>' . $timestamp . '</wsu:Created>' .
            '<wsu:Expires>' . $expiration . '</wsu:Expires>' .
            '</wsu:Timestamp>' .
            '<wsse:UsernameToken>' .
            '<wsse:Username>' . $this->options['username'] . '</wsse:Username>' .
            '<wsse:Password Type="' . Security::WSUTP . '#' . $this->options['password_type'] . '">' . $password . '</wsse:Password>' .
            '<wsse:Nonce EncodingType="' . Security::WSMS . '#' .Security::BASE64_BINARY.'">' . $nonce . '</wsse:Nonce>';

        if ($this->options['password_type'] === Security::PASSWORD_DIGEST) {
            $xml .= '<wsu:Created xmlns:wsu="' . Security::WSU . '">' . $timestamp . '</wsu:Created>';
        }

        $xml = $xml . '</wsse:UsernameToken></wsse:Security>';

        return new \SoapHeader(
            Security::WSSE,
            'Security',
            new \SoapVar($xml, XSD_ANYXML),
            true
        );
    }
}