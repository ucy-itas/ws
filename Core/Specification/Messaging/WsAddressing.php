<?php
namespace Ucy\Iss\Component\Ws\Core\Specification\Messaging;

use Ucy\Iss\Component\Ws\Core\Specification;
use Ucy\Iss\Component\Ws\Core\SpecificationInterface;
use Ucy\Iss\Component\Ws\Core\Specification\Messaging;

/**
 * Class WsAddressing
 * @package Ucy\Iss\Component\Ws\Core\Specification
 */
class WsAddressing extends Specification
{
    public function __construct($options)
    {
        // TODO: Require endpoint and action_uri parameters
        $this->setName(Messaging::WS_ADDRESSING);
        parent::__construct($options);
    }

    /**
     * @param array $runtime_options
     * @return \SoapHeader | \SoapHeader[]
     */
    public function generateHeaders(array $runtime_options = array())
    {
        $action = new \SoapHeader(Messaging::WSA, 'Action', $this->options['action_uri'] . $runtime_options['function_name'], true);
        $to = new \SoapHeader(Messaging::WSA, 'To', $this->options['endpoint'], true);
        return array($action, $to);
    }
}