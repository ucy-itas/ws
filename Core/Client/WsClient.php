<?php
namespace Ucy\Iss\Component\Ws\Core\Client;

use Ucy\Iss\Component\Ws\Core\SpecificationInterface;

class WsClient extends \SoapClient
{

    protected array $headers = [];

    protected array $extra_headers = [];

    /**
     * @var SpecificationInterface[]
     */
    protected array $specifications = [];

    public function __call($name, $args)
    {
        $runtime_options = [
            'function_name' => $name,
            'arguments' => $args
        ];

        $this->setHeaders([]);
        foreach($this->specifications as $key => $specification){
            $this->addHeader($key, $specification->generateHeaders($runtime_options));
        }

        return parent::__soapCall($name, $args, null, $this->getSoapHeaders());
    }

    public function setSpecification(SpecificationInterface $specification)
    {
        $this->specifications[$specification->getName()] = $specification;
    }

    public function getSpecificationByName(string $name): ?SpecificationInterface
    {
        return (isset($this->specifications[$name])) ? $this->specifications[$name] : null;
    }

    /**
     * @param string $key
     * @param \SoapHeader|\SoapHeader[] $header
     */
    public function addHeader(string $key, $header)
    {
        $this->headers[$key] = $header;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    /**
     * @param string $key
     * @param \SoapHeader|\SoapHeader[] $header
     */
    public function addExtraHeader(string $key, $header)
    {
        $this->extra_headers[$key] = $header;
    }

    public function getExtraHeaders(): array
    {
        return $this->extra_headers;
    }

    public function setExtraHeaders(array $extra_headers)
    {
        $this->extra_headers = $extra_headers;
    }

    private function getSoapHeaders(): array
    {
        $headers = [];
        foreach (range(0, 1, 1) as $iteration) {
            $load_headers = array_merge($this->getHeaders(), $this->getExtraHeaders());

            foreach ($load_headers as $key => $header) {
                if (is_null($header)) {
                    continue;
                }

                if ($header instanceof \SoapHeader) {
                    $headers[$key] = $header;
                    continue;
                }

                if (is_array($header)) {
                    foreach ($header as $sub_key => $sub_header) {
                        if ($sub_header instanceof \SoapHeader) {
                            $headers[$key . $sub_key] = $sub_header;
                        }
                    }
                }
            }
        }
        return $headers;
    }
}
