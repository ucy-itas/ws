<?php
namespace Ucy\Iss\Component\Ws\Core;


interface SpecificationInterface
{
    /**
     * @return \SoapHeader | array
     */
    public function generateHeaders(array $runtime_options = array());

    public function getOptions(): array;

    public function setOptions(array $options): SpecificationInterface;

    public function getName(): string;

    public function setName(string $name): SpecificationInterface;
}
