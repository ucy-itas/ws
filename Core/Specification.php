<?php
namespace Ucy\Iss\Component\Ws\Core;


abstract class Specification implements SpecificationInterface
{
    protected array $options;
    protected string $name;

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): SpecificationInterface
    {
        $this->options = $options;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): SpecificationInterface
    {
        $this->name = $name;
        return $this;
    }

}
